# Event Management
The event management module allows you to display upcoming events in a custom
block.

# Requirements
Following modules are required;
* addressfield
* date
* features

# Installation
To install, copy the event-management folder in your 'sites/all/modules'
directory. Go to Administer -> modules and look for 'Event Management' and
enable this module.

# Configuration
Once module is enabled
- it will create a custom content 'Event'.
- it will also create a custom block 'Upcoming Events'.

Add upto 3 events by going to 'Content -> Add Content -> Event'.

Now go to 'Structure -> Blocks' and look for 'Upcoming Events' and assign a
region to this block.

Refresh home page and you will see a block with most recent 3 events with a link
to full event detail page.

# Use Case
Following use case is implemented in this module i.e

Create a new content type in Drupal 7 called ​Event​.

The new ​Event​ content type should have four fields:
a) Event name (type: Text)
b) Event details (type: Longtext)
c) Event date (type: Date. Use the ​Date​ module) (Include a start and end date)
d) Event location (type: Postal address. Use the ​Address Field​ module)

Write a custom module that updates the Event node title when an Event is
added or saved.
a) The Event title should be a combination of the Events fields as such:
“​Event name - Event date (day of the week) Event location (city)”.
b) Example: My birthday - Wednesday Chicago
c) The day of the week should be based off of the Event start date’s day.
d) After the node has been created, only run the update code if any of the
fields have been changed when saving the node.
e) The module should create a custom block on the homepage with three upcoming
Events, including their details, location, and dates in order of most recent
first. Set the title of the block to ​Upcoming Events​. The title should use h3
markup. Dates should appear italicized.

Provide this module so as if either downloaded or cloned, it should perform
all actions mentioned above.

# Current maintainer
Baber Abbasi - https://www.drupal.org/user/3385074
